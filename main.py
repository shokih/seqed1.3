from __future__ import unicode_literals
from PyQt4 import QtGui, QtCore, uic
from flash import *
from spinecho import *
from inversionrecovery import *
from FID import *
import sys

class SeqedWindow(QtGui.QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowIcon(QtGui.QIcon('mainicon.png'))
        self.path = path.dirname(path.realpath(__file__))
        self.ui = uic.loadUi(self.path + '/MainWidget.ui', self)
        self.seq = Sequence(self.ui)
        self.changeseqtype()
        self.riset.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.te.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.tr.valueChanged.connect(lambda: self.uvariables([0, 0, 0, 0]))
        self.noviews.valueChanged.connect(lambda: self.uvariables([0, 0, 0, 0]))
        self.slice_steps.valueChanged.connect(lambda: self.uvariables([0, 1, 0, 0]))
        self.savecsv.clicked.connect(lambda: self.writecsv(self.seq))
        self.ti.valueChanged.connect(lambda: self.uvariables([1, 1, 0, 0]))
        self.seqtype.currentIndexChanged.connect(lambda: self.changeseqtype())
        self.acqnum.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.flipa.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.rfbw.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.acqw.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.slicethickness.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.pfov.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.rfov.valueChanged.connect(lambda: self.uvariables([1, 1, 1, 1]))
        self.pulse_amplitude.valueChanged.connect(lambda: self.uvariables([0, 0, 0, 0]))
        self.pulse_frequency.valueChanged.connect(lambda: self.uvariables([0, 0, 0, 0]))

    def writecsv(self, sequence):
        fn = QtGui.QFileDialog.getSaveFileName(self, 'Save csv', self.path + '/Sequences', 'CSV (*.csv)')
        print(fn)
        if fn:
            sequence.writetocsv(fn)

    def block(self, state):
        self.riset.blockSignals(state)
        self.te.blockSignals(state)
        self.tr.blockSignals(state)
        self.noviews.blockSignals(state)
        self.savecsv.blockSignals(state)
        self.ti.blockSignals(state)
        self.acqnum.blockSignals(state)
        self.flipa.blockSignals(state)
        self.pulse_amplitude.blockSignals(state)
        self.pulse_frequency.blockSignals(state)
        self.gradx_offset.blockSignals(state)
        self.grady_offset.blockSignals(state)
        self.gradz_offset.blockSignals(state)
        self.b0_offset.blockSignals(state)
        self.rfbw.blockSignals(state)
        self.acqw.blockSignals(state)
        self.slicethickness.blockSignals(state)
        self.pfov.blockSignals(state)
        self.rfov.blockSignals(state)

    def uvariables(self, upd):
        self.ui.widget_RF.disperror(' ')
        self.seq.get_parameters(self.ui)
        if upd[0] == 1:
            self.ui.widget_RF.update_figure(self.seq.Dictionary_Sequence, 'RF')
        if upd[1] == 1:
            self.ui.widget_Slice.update_figure(self.seq.Dictionary_Sequence, 'Slice')
        if upd[2] == 1:
            self.ui.widget_Phase.update_figure(self.seq.Dictionary_Sequence, 'Phase')
        if upd[3] == 1:
            self.ui.widget_Read.update_figure(self.seq.Dictionary_Sequence, 'Read')

    def changeseqtype(self):
        self.block(True)
        if self.ui.seqtype.currentText() == 'FID':
            self.seq = FID(self.ui)
        elif self.ui.seqtype.currentText() == 'Gradient Echo':
            self.seq = Flash(self.ui)
        elif self.ui.seqtype.currentText() == 'Spin Echo':
            self.seq = Spinecho(self.ui)
            self.ui.slice_steps.setMinimum(1)
        elif self.ui.seqtype.currentText() == 'Inversion Recovery':
            self.seq = invrecovery(self.ui)
        self.seq.toggledisplay(self.ui)
        self.uvariables([1, 1, 1, 1])
        self.block(False)

    def closeEvent(self, event):
        self.seq.writetocsv(self.path + '/Sequences/default.csv')
        self.seq.writetocsv(self.path + '/Sequences/' + self.ui.seqtype.currentText() + '.csv')

if __name__ == "__main__":
    qApp = QtGui.QApplication(sys.argv)
    aw = SeqedWindow()
    p = aw.palette()
    p.setColor(aw.backgroundRole(), QtCore.Qt.white)
    aw.setPalette(p)
    aw.show()
    sys.exit(qApp.exec_())
    qApp.exec_()