from grad import Grad
from sequence import Sequence
from gradientamplitude import *

class Flash(Sequence):
    def __init__(self, gui):
        super(Flash, self).__init__(gui, 'Gradient Echo')
        self.name = Flash.__name__

    def get_parameters(self, gui):
        phaset = gui.riset.value() + (1/gui.rfbw.value()) * rfwidth(gui) + 2 * gui.riset.value()
        acqt = gui.riset.value() + (1/gui.rfbw.value()) * rfwidth(gui) / 2 + gui.te.value() - gui.ui.acqw.value()/2
        phasew = acqt - 2 * gui.riset.value() - phaset
        if gui.slice_steps.value() > 1:
            self.threed = 1
            self.slice = [Grad(phaset, phasew, 3.5)]
        else:
            self.threed = 0
            self.slice = [Grad(gui.riset.value(), (1/gui.rfbw.value()) * rfwidth(gui), 3.5, postwidth=phasew)]
        self.rf = [Grad(gui.riset.value(), (1/gui.rfbw.value()) * rfwidth(gui), gui.flipa.value())]
        self.phase = [Grad(phaset, phasew, phaseamp(gui, phasew))]
        self.read = [Grad(acqt, gui.ui.acqw.value(), readamp(gui), prewidth=phasew)]
        self.acq = [Grad(acqt, gui.ui.acqw.value(), 0)]
        self.phasecrusheramp = 0
        for i in range(gui.acqnum.value() - 1):
            rep = gui.ui.acqw.value() + gui.riset.value() * 2
            self.phasecrusheramp = 0.8
            self.read.append(Grad(acqt + (i + 1) * rep, gui.ui.acqw.value(), -1 * self.read[-1].amplitude))
            self.acq.append(Grad(acqt + (i + 1) * rep, gui.ui.acqw.value(), 0))
        self.AcquisitionPattern(gui)
        self.prepareScan()


    def toggledisplay(self, gui):
        gui.ti.hide()
        gui.tilabel.hide()
        gui.flipa.show()
        gui.flipalabel.show()
        gui.acqnum.show()
        gui.acqnumlabel.show()