from grad import *
from sequence import *

class Spinecho(Sequence):
    def __init__(self, gui):
        super(Spinecho,self).__init__(gui, 'Spin Echo')
        self.name=Spinecho.__name__

    def get_parameters(self, gui):
        #self.rf = [(Grad(gui.ui.rft.value(),gui.ui.rfw.value(),90))]
        #self.rf.append(Grad(gui.ui.rft.value() + (gui.ui.te.value()/2.0), gui.ui.rfw.value(), 180))
        #self.slice = [(Grad(gui.ui.rft.value() - gui.ui.riset.value(), gui.ui.rfw.value(), gui.ui.sliceamp.value(), postwidth = gui.ui.phasew.value() ))]
        #self.slice.append(Grad(gui.ui.rft.value() + (gui.ui.te.value()/2.0) - 0.125 - gui.ui.riset.value(), gui.ui.rfw.value(), gui.ui.sliceamp.value(), crushwidth = 0.125, crushamp = 3.5 ))
        #self.phase = [(Grad(gui.ui.phaset.value(), gui.ui.phasew.value(), gui.ui.phaseamp.value()))]
        #self.read = [(Grad(gui.ui.rft.value() + gui.ui.te.value() + (gui.ui.rfw.value() / 2.0) - (gui.ui.acqw.value()/2), gui.ui.acqw.value(), gui.ui.readamp.value(), prewidth = gui.ui.phasew.value()))]
        #self.acq = [(Grad(gui.ui.rft.value() + gui.ui.te.value() + (gui.ui.rfw.value() / 2.0) - (gui.ui.acqw.value()/2),gui.ui.acqw.value(), gui.ui.readamp.value()))]
        #self.acqnum = 1
        #self.tr = gui.ui.tr.value()
        #self.te = gui.ui.te.value()
#        for i in range(gui.acqnum.value() - 1):
#            rep = gui.acqw.value() + gui.riset.value() * 2
#            self.phasecrusheramp = 0.8
#            self.read.append(Grad(readt + (i + 1) * rep, gui.acqw.value(), -1 * self.read[-1].amplitude))
#            self.acq.append(Grad(readt + (i + 1) * rep, gui.acqw.value(), 0))
        self.AcquisitionPattern(gui)
        self.prepareScan()

    def toggledisplay(self, gui):
            gui.ti.hide()
            gui.tilabel.hide()
            gui.flipa.hide()
            gui.flipalabel.hide()
            gui.slice_steps.setMinimum(1)
