__author__ = 'shokih'

def readamp(gui):
    #[length, fov, matrix size] at 1V
    default = [8, 150, 192]
    current = [gui.ui.acqw.value(), gui.ui.rfov.value(), 192]
    amp = default[0]/current[0] * default[1] / current[1]
    return amp

def phaseamp(gui, phaselength):
    #[length, fov, matrix size] at 1V
    default = [8, 150, 192]
    current = [phaselength, gui.ui.pfov.value(), gui.ui.noviews.value()]
    amp = default[0]/current[0] * default[1] / current[1] * default[2] / current[2]
    return amp

def readfov(amp, length, matrix=192):
    default = [8, 150, 192]
    current = [0, length, matrix]
    fov = default[1]/current[1]  * default[0] / amp
    return fov

def phasefov(amp, length, matrix=192):
    default = [8, 150, 192]
    current = [0, length, matrix]
    fov = default[1]/current[1]  * default[0] / amp
    return fov

def rfwidth(gui):
    if gui.ui.seqtype.currentText() == 'Gradient Echo':
        encodewidth = gui.ui.te.value() - gui.ui.acqw.value()/2 - 2/gui.ui.rfbw.value()
        if encodewidth > 1:
            N = 4
        else:
            encodewidth = encodewidth + 1/gui.ui.rfbw.value()
            if encodewidth > 1:
                N = 2
            else:
                gui.widget_RF.disperror('Error: TE too short')
                N = 1
    else:
        N = 4
    return N







