from grad import Grad
from sequence import Sequence

class FID(Sequence):
    def __init__(self, gui):
        super(FID, self).__init__(gui, 'FID')
        self.name = FID.__name__
        self.get_parameters(gui)

    def get_parameters(self, gui):
        self.rf = [Grad(gui.riset.value(), (1/gui.rfbw.value()) * 2, gui.flipa.value())]
        self.acq = [Grad(gui.riset.value() + 1/gui.rfbw.value() + gui.te.value() - gui.ui.acqw.value()/2, gui.ui.acqw.value(), 0)]
        self.AcquisitionPattern(gui)
        self.prepareScan()

    def toggledisplay(self, gui):
        gui.ti.hide()
        gui.tilabel.hide()
        gui.flipa.show()
        gui.flipalabel.show()
        gui.slice_steps.setMinimum(1)