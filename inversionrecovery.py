from grad import *
from sequence import *

class invrecovery(Sequence):
    def __init__(self, gui):
        super(invrecovery,self).__init__(gui, 'Inversion Recovery')
        self.name=invrecovery.__name__

    def get_parameters(self, gui):
        #self.rf = [Grad(gui.ui.riset.value(), gui.ui.rfw.value(), 180)]
        #self.rf.append(Grad(gui.ui.ti.value() - (gui.ui.rfw.value()/2), gui.ui.rfw.value(), 90))
        #self.rf.append(Grad((gui.ui.te.value()/2) - (gui.ui.rfw.value()/2) + gui.ui.ti.value(),gui.ui.rfw.value(),180))
        #self.slice= [Grad(gui.ui.riset.value(), gui.ui.rfw.value(), gui.ui.sliceamp.value())]
        #self.slice.append(Grad(gui.ui.ti.value() - gui.ui.rfw.value()/2, gui.ui.rfw.value(), gui.ui.sliceamp.value(), postwidth = gui.ui.phasew.value()))
        #self.slice.append(Grad(gui.ui.riset.value() + gui.ui.te.value()/2 - gui.ui.rfw.value()/2 + gui.ui.ti.value() - 0.525, gui.ui.rfw.value(), gui.ui.sliceamp.value(), crushwidth = 0.125, crushamp = 3 ))
        #self.phase=[Grad(gui.ui.phaset.value(), gui.ui.phasew.value(), gui.ui.phaseamp.value())]
        #self.read=[Grad(gui.ui.riset.value() + gui.ui.rfw.value()/2 + gui.ui.te.value()/2 + gui.ui.ti.value() - gui.ui.acqw.value()/2, gui.ui.acqw.value(), gui.ui.readamp.value(), prewidth = gui.ui.phasew.value())]
        #self.acq=[Grad(gui.ui.riset.value() + gui.ui.rfw.value()/2 + gui.ui.te.value()/2 + gui.ui.ti.value() - gui.ui.acqw.value()/2,gui.ui.acqw.value(), 1)]
        #self.AcquisitionPattern(gui)
        self.prepareScan()

    def toggledisplay(self, gui):
        gui.ti.show()
        gui.tilabel.show()
        gui.acqnum.hide()
        gui.acqnumlabel.hide()
        gui.slice_steps.setMinimum(1)