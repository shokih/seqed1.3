import csv
from os import path
from numpy import *
from gradientamplitude import *

class Sequence(object):
    def __init__(self, gui, seqtype = 0):
        self.Dictionary_Sequence = {}
        self.gui = gui
        gui.gradx_offset.valueChanged.connect(self.up_offset)
        gui.grady_offset.valueChanged.connect(self.up_offset)
        gui.gradz_offset.valueChanged.connect(self.up_offset)
        gui.b0_offset.valueChanged.connect(self.up_offset)
        current_path = path.dirname(path.realpath(__file__)) + '/Sequences/'

        if seqtype:
            self.readfromcsv(current_path + seqtype + '.csv')
        else:
            self.readfromcsv(current_path + 'default.csv')
        self.readfromdict(gui)

        self.threed = 0
        self.rf = []
        self.slice = []
        self.phase = []
        self.read = []
        self.acq = []
        self.ti = None
        self.tr = None
        self.te = None
        self.rt = None
        self.slicecrusheramp = 0
        self.phasecrusheramp = 0
        self.readcrusheramp = 0

    def readfromdict(self, gui):
        gui.block(True)
        try:
            gui.seqtype.setCurrentIndex(gui.ui.seqtype.findText(self.Dictionary_Sequence['Sequence Type']))
            gui.tr.setValue(self.Dictionary_Sequence['TR'])
            gui.te.setValue(self.Dictionary_Sequence['TE'])
            gui.riset.setValue(self.Dictionary_Sequence['Rise Time'])
            gui.acqnum.setValue(self.Dictionary_Sequence['Acquisition Number'])
            gui.noviews.setValue(self.Dictionary_Sequence['Phase Steps'])
            gui.acqw.setValue(self.Dictionary_Sequence['Acquisition Length'][0])
            gui.ti.setValue(self.Dictionary_Sequence['TI'])
            gui.flipa.setValue(self.Dictionary_Sequence['Pulse Angle'][0])
            gui.noviews.setValue(self.Dictionary_Sequence['Phase Steps'])
            gui.slice_steps.setValue(self.Dictionary_Sequence['Slice Steps'])
            gui.pulse_amplitude.setValue(self.Dictionary_Sequence['Pulse Value 90'])
            gui.pulse_frequency.setValue(self.Dictionary_Sequence['Pulse Frequency'])
            gui.gradx_offset.setValue(self.Dictionary_Sequence['Offset'][0])
            gui.grady_offset.setValue(self.Dictionary_Sequence['Offset'][1])
            gui.gradz_offset.setValue(self.Dictionary_Sequence['Offset'][2])
            gui.b0_offset.setValue(self.Dictionary_Sequence['Offset'][3])
            gui.rfov.setValue(readfov(self.Dictionary_Sequence['Read Amplitude'][1], self.Dictionary_Sequence['Read Length'][1]))
            gui.pfov.setValue(phasefov(self.Dictionary_Sequence['Phase Amplitude'][0], self.Dictionary_Sequence['Phase Length'][0]))
            gui.block(False)
        except:
            gui.block(False)

    def prepareScan(self):
        self.Dictionary_Sequence.update({'Pulse Start': [x.time for x in self.rf]})
        self.Dictionary_Sequence.update({'Pulse Length': [x.width for x in self.rf]})
        self.Dictionary_Sequence.update({'Pulse Angle': [x.amplitude for x in self.rf]})
        self.Dictionary_Sequence.update({'Pulse Number': len(self.rf)})

        self.Dictionary_Sequence.update({'Slice Start': []})
        self.Dictionary_Sequence.update({'Slice Length': []})
        self.Dictionary_Sequence.update({'Slice Amplitude': []})
        self.Dictionary_Sequence.update({'Slice Lobes Amplitude': []})
        self.Dictionary_Sequence.update({'Slice Lobes Length': []})

        for i in range(len(self.slice)):
            if hasattr(self.slice[i], 'pretime'):
                self.Dictionary_Sequence['Slice Start'].append(self.slice[i].pretime)
                self.Dictionary_Sequence['Slice Length'].append(self.slice[i].prewidth)
                self.Dictionary_Sequence['Slice Amplitude'].append(self.slice[i].preamplitude)
                self.Dictionary_Sequence['Slice Lobes Amplitude'].append(0.)
                self.Dictionary_Sequence['Slice Lobes Length'].append(0.)
            self.Dictionary_Sequence['Slice Start'].append(self.slice[i].time)
            self.Dictionary_Sequence['Slice Length'].append(self.slice[i].width)
            self.Dictionary_Sequence['Slice Amplitude'].append(self.slice[i].amplitude)
            self.Dictionary_Sequence['Slice Lobes Length'].append(self.slice[i].crushwidth)
            self.Dictionary_Sequence['Slice Lobes Amplitude'].append(self.slice[i].crushamp)
            if hasattr(self.slice[i], 'posttime'):
                self.Dictionary_Sequence['Slice Start'].append(self.slice[i].posttime)
                self.Dictionary_Sequence['Slice Length'].append(self.slice[i].postwidth)
                self.Dictionary_Sequence['Slice Amplitude'].append(self.slice[i].postamplitude)
                self.Dictionary_Sequence['Slice Lobes Amplitude'].append(0.)
                self.Dictionary_Sequence['Slice Lobes Length'].append(0.)
        self.Dictionary_Sequence.update({'Slice Number': len(self.Dictionary_Sequence['Slice Start'])})

        #Phase Update
        self.Dictionary_Sequence.update({'Phase Start': [x.time for x in self.phase]})
        self.Dictionary_Sequence.update({'Phase Length': [x.width for x in self.phase]})
        self.Dictionary_Sequence.update({'Phase Amplitude': [x.amplitude for x in self.phase]})
        self.Dictionary_Sequence.update({'Phase Lobes Amplitude': [0. for x in self.phase]})
        self.Dictionary_Sequence.update({'Phase Lobes Length': [0. for x in self.phase]})
        self.Dictionary_Sequence.update({'Phase Number': len(self.phase)})

        self.Dictionary_Sequence.update({'Read Start': []})
        self.Dictionary_Sequence.update({'Read Length': []})
        self.Dictionary_Sequence.update({'Read Amplitude': []})
        self.Dictionary_Sequence.update({'Read Lobes Amplitude': []})
        self.Dictionary_Sequence.update({'Read Lobes Length': []})
        for i in range(len(self.read)):
            if hasattr(self.read[i], 'pretime'):
                self.Dictionary_Sequence['Read Start'].append(self.read[i].pretime)
                self.Dictionary_Sequence['Read Length'].append(self.read[i].prewidth)
                self.Dictionary_Sequence['Read Amplitude'].append(self.read[i].preamplitude)
                self.Dictionary_Sequence['Read Lobes Amplitude'].append(0.)
                self.Dictionary_Sequence['Read Lobes Length'].append(0.)
            self.Dictionary_Sequence['Read Start'].append(self.read[i].time)
            self.Dictionary_Sequence['Read Length'].append(self.read[i].width)
            self.Dictionary_Sequence['Read Amplitude'].append(self.read[i].amplitude)
            self.Dictionary_Sequence['Read Lobes Amplitude'].append(0.)
            self.Dictionary_Sequence['Read Lobes Length'].append(0.)
            if hasattr(self.read[i], 'posttime'):
                self.Dictionary_Sequence['Read Start'].append(self.read[i].posttime)
                self.Dictionary_Sequence['Read Length'].append(self.read[i].postwidth)
                self.Dictionary_Sequence['Read Amplitude'].append(self.read[i].postamplitude)
                self.Dictionary_Sequence['Read Lobes Amplitude'].append(0.)
                self.Dictionary_Sequence['Read Lobes Length'].append(0.)
        self.Dictionary_Sequence.update({'Read Number': len(self.Dictionary_Sequence['Read Start'])})

        self.Dictionary_Sequence.update({'Acquisition Start': [x.time for x in self.acq]})
        self.Dictionary_Sequence.update({'Acquisition Length': [x.width for x in self.acq]})
        self.Dictionary_Sequence.update({'Acquisition Number': len(self.acq)})

        self.Dictionary_Sequence.update({'TI': self.gui.ti.value()})
        self.Dictionary_Sequence.update({'TR': self.gui.tr.value()})
        self.Dictionary_Sequence.update({'TE': self.gui.te.value()})
        self.Dictionary_Sequence.update({'Rise Time': self.gui.riset.value()})
        self.Dictionary_Sequence.update({'Kspace Index': self.i_kspace.tolist()})
        self.Dictionary_Sequence.update({'Oblique Angle': [0, 90, 180, 90]})
        self.Dictionary_Sequence.update({'Slice Pattern': self.slice_pattern.tolist()})
        self.Dictionary_Sequence.update({'Phase Pattern': self.phase_pattern.tolist()})
        self.Dictionary_Sequence.update({'Phase Index': self.i_phase_seq.tolist()})
        self.Dictionary_Sequence.update({'Slice Index': self.i_slice_seq.tolist()})
        self.Dictionary_Sequence.update({'Slice Steps': self.gui.ui.slice_steps.value()})
        self.Dictionary_Sequence.update({'Phase Steps': self.gui.ui.noviews.value()})
        self.Dictionary_Sequence.update({'Pulse Pattern': self.pulse_pattern.tolist()})
        self.Dictionary_Sequence.update({'Pulse Frequency': self.gui.pulse_frequency.value()})
        self.Dictionary_Sequence.update({'Pulse Value 90': self.gui.pulse_amplitude.value()})
        self.Dictionary_Sequence.update({'Three Dimensional' : self.threed})
        self.Dictionary_Sequence.update({'Phase Crusher Amplitude': self.phasecrusheramp})
        self.Dictionary_Sequence.update({'Slice Crusher Amplitude': self.slicecrusheramp})
        self.Dictionary_Sequence.update({'Read Crusher Amplitude': self.readcrusheramp})
        self.Dictionary_Sequence.update({'Sequence Type': self.gui.seqtype.currentText()})
        self.up_offset()

    def up_offset(self):
        self.Dictionary_Sequence.update({'Offset': [self.gui.gradx_offset.value(), self.gui.grady_offset.value(), self.gui.gradz_offset.value(), self.gui.b0_offset.value()]})

    def writetocsv(self, fn):
        keys = list(self.Dictionary_Sequence.keys())
        with open(fn, 'w') as out:
            csv_out = csv.writer(out, lineterminator='\n')
            for key_ in sorted(keys):
                val_ = self.Dictionary_Sequence[key_]
                csv_out.writerow([key_, val_])

    def readfromcsv(self, fn):
        reader = csv.reader(open(fn))
        for row in reader:
            try:
                self.Dictionary_Sequence[row[0]] = eval(row[1])
            except NameError:
                self.Dictionary_Sequence[row[0]] = row[1]
            except SyntaxError:
                self.Dictionary_Sequence[row[0]] = row[1]

    def AcquisitionPattern(self, gui):
        self.phase_steps = gui.noviews.value()
        self.slice_steps = gui.slice_steps.value()
        self.nbr_acq = gui.acqnum.value()
        self.pulse_pattern = zeros([self.slice_steps], dtype=float64)
        for i in range(self.slice_steps):
            if i % 2 == 0:
                self.pulse_pattern[i] = i // 2 * 750
            else:
                self.pulse_pattern[i] = - (i + 1) // 2 * 750
        self.phase_pattern = linspace(1, -1, num=self.phase_steps)
        if self.slice_steps > 1:
            self.slice_pattern = linspace(1, -1, num=self.slice_steps)
        else:
            self.slice_pattern = ones([self.slice_steps], dtype=float64)
        self.i_phase_seq = zeros([self.phase_steps * self.slice_steps], dtype=int16)
        self.i_slice_seq = zeros([self.phase_steps * self.slice_steps], dtype=int16)
        self.i_kspace = zeros([self.nbr_acq, self.phase_steps, self.slice_steps, 2], dtype=int16)
        for i in range(self.phase_steps * self.slice_steps):
            self.i_phase_seq[i] = i // self.slice_steps
            self.i_slice_seq[i] = i % self.slice_steps
        for i in range(self.nbr_acq):
            for j in range(self.phase_steps):
                for k in range(self.slice_steps):
                    self.i_kspace[i, j, k, 0] = j
                    self.i_kspace[i, j, k, 1] = k